# Syrebro

Symbolic Execution Shell with various plugins. 

Syrebro is a simple shell that gives you a live interface to a symbolic execution session over a binary.
Syrebro will offer a plugin driven archiecture which allows you to respond to symbolic execution events and offer extended functionality.

Built on Angr it offers easy interface to introspecting Consumer and Off the Shelf binaries and as it stands Syrebro can do the following:
*  Step through symbolic execution one step at a time (by using the 'n' command)
*  Step through symbolic execution until a given condition is meant either meeting a number of deadended states, or a number of active states i.e. execute until n amount of active / deadended states are found
*  Produce png / svg / jpg output of symbolic execution graphs showing the current complexity of the state history for a binary
*  Produce gifs of the symbolic execution journey, replaying the state growth and connection density overtime (work in progress)


# Usage

Loading a binary into Syrebro is easy, you just specify it using either the -b / --binary command line switches, or with the 'load -b / --binary [binary name]' command as follows:

```
>$ ./main.py -b /usr/bin/gdb
syrebro>n
```

In the above example we issue a 'n' command which steps execution forward exactly 1 step, this is to make sure the binary loaded correctly and can be executed symbolically.
Or we can load the binary as follows:
```
>$ ./main.py
syrebro>load -b /usr/bin/gdb
```

if all goes well you should see the following prompt:
```
*syrebro:/usr/bin/gdb (1)[a:1 d:0]{<SimState @ 0x555555e000b0>}>
```
The command prompt shows some useful information

# Commands

Currently Syrebro supports a some simple functionality the following few examples show how to use them.

## Stepping through execution
If you'd like to step through execution until a given number of active states are found (at least), issue the following command:
```
*syrebro:binary name (1)[a:1 d:0]{<SimState @ 0x555555e000b0>}> n --active 2
*syrebro:binary name (1)[a:1 d:0]{<SimState @ 0x555555e000b0>}> n --a 2
```
Which will step execution until it see's at least 2 active states. Similarly for deadended states:

```
*syrebro:binary name (1)[a:1 d:0]{<SimState @ 0x555555e000b0>}> n --deadended 2
*syrebro:binary name (1)[a:1 d:0]{<SimState @ 0x555555e000b0>}> n --d 2
```
Which will step execution until we see at least 2 deadended states

#Graphing Symbolic Execution history
Syrebro can also output neat graphs of the current state of the symbolic execution history, to do this use the 'show graph [filename]' command as follows:
```
*syrebro:binary name (1)[a:1 d:0]{<SimState @ 0x555555e000b0>}>show graph filename
```

This will produce a file called filename.svg that contains your neat graph. Here are a few examples:
* ![GDB approximately 25 steps into execution](https://gitlab.com/k3170makan/syrebro/-/blob/master/gdb.png)
* ![Objdump approximately 25 steps into execution](https://gitlab.com/k3170makan/syrebro/-/blob/master/objdump.png)

# References and Reading
