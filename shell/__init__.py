#!/usr/bin/python3

import traceback
import argparse
import time
import sys
from .plugins import innerangr

"""
Interactive shell for navigating a symbolic execution run

"""

class Parser:
	def __init__(self,binary=None):
		self.load_parser = argparse.ArgumentParser()
		self.load_parser.add_argument("-b","--binary",help="Binary to parse")	
		self.load_parser.add_argument("-D","--directed_exec",help="Use directed symbolic execution",action='store_true',default=False)	
		self.load_parser.add_argument("-c","--arg_count",help="Number of arguments",default=3)	
		self.load_parser.add_argument("-l","--arg_length",help="Bit length of each argument",default=16)	
		self.load_parser.add_argument("-L","--limit",help="maximum number of states to keep alive during depth first search",type=int,default=1)	
	
		self.next_parser = argparse.ArgumentParser()
		self.next_parser.add_argument("-t","--target",help="execute until this target address is reached",default="")	
		self.next_parser.add_argument("-a","--actives",help="execute until this many states are active",default=-1,type=int)	
		self.next_parser.add_argument("-d","--deadended",help="execute until this many states are deadended",default=-1,type=int)	

		self.history_parser = argparse.ArgumentParser()
		self.history_parser.add_argument("-a","--active",help="show history for this active state",default=0)	


		self.constraint_history_parser = argparse.ArgumentParser()
		self.constraint_history_parser.add_argument("-a","--active_index",help="show constraint history for this active state",default=0)	


		self.show_parser = argparse.ArgumentParser()
		self.show_parser.add_argument("-g","--graph",help="show graph",action='store_true',default=False)
		self.show_parser.add_argument("--gifgraph",help="show graph as fig",action='store_true',default=False)
		self.show_parser.add_argument("--graph_filename",help="graph plot filename to save it to",default="syrebro")
		self.show_parser.add_argument("--graph_labels",help="show address labels on nodes",default=False)
		self.show_parser.add_argument("--graph_dpi",help="pixel density for the graph output",default=1500)
		self.show_parser.add_argument("--graph_steps",help="Number of steps to execute before drawing graph",default=0,type=int)
		self.show_parser.add_argument("--cfg",help="draw a cfg plot and output it to an image file")
		self.binary = binary
		self.init_loaded = False
	def line_parse(self,line,innerangr=None):
		if self.binary and not(self.init_loaded):
			self.init_loaded = True
			innerangr.set_target(self.binary)	
			
		for token in self.__parse(line):
			#print("line_parse TOKEN:",token)
			triggered = False
			for plugin_name in plugins.PLUGINDICT:
				if token == plugin_name:
					triggered = True
					plugins.PLUGINDICT[plugin_name].run()
					break
			if not(triggered):
				self.native_line_parse(line,innerangr)	
				break

		return	

	def __parse(self,line):
		return [i for i in line.split("-") if len(i) > 0]

	def native_line_parse(self,line,innerangr=None):
		print(line.split(';'))
		for token in line.split(';'):
			#print("native-line-parse TOKEN:",token)
			if "exit" in token:
				print("[*] bye bye :)")
				sys.exit(0)		

			if "load" in token:
				load_args = self.load_parser.parse_args(token[len("load"):].split())
				innerangr.set_target(load_args.binary)	
				return

			if "show" in token:
				print("[*] show...")
				show_args = self.show_parser.parse_args(token[len("show"):].split())
				if show_args.graph:
					innerangr.plot_digraph(filename=show_args.graph_filename,
														steps=show_args.graph_steps,
														dpi=show_args.graph_dpi,
														labels=show_args.graph_labels)	
					return
				elif show_args.gifgraph:
					innerangr.plot_gifdigraph(filename=show_args.graph_filename,
														steps=show_args.graph_steps)	
					return
				"""
				commands = token[4:].split(" ")
				if "gifgraph" in commands:
					filename = token[4+9:].split(" ")
					args = token[4+6+len(filename)].split(" ")
					print(filename,args)
					innerangr.plot_gifdigraph()

				if "graph" in commands:
					filename = token[4+6:].split(" ")
					if filename[1] != '':
						innerangr.plot_digraph(filename[1]+".png")
				
				"""	
			if "n" in token: #1 step
				if len(token) > 1:
					next_args = self.next_parser.parse_args(token[len("n"):].split())
					print("[*] stepping execution...")
					return innerangr.step_until(actives=next_args.actives,deadended=next_args.deadended)	
				else:
					return innerangr.step_state()
			if "P" in token: #1 prev step
				pass
			if "L" in token: #move cursor left
				pass
			if "R" in token: #move cursor right
				pass
			if "D" in token: #delete state (and children)
				pass
			
			if "H" in token or "history" in token: #delete state (and children)
				hist_args = None
				if "history" in token:
					hist_args = self.history_parser.parse_args(token[len("history"):].split())	
					innerangr.print_history(active_index=hist_args.active_index)
				else:
					innerangr.print_history()
					
			if "C" in token or "constrs" in token: #delete state (and children)
				const_args = None
				if "constr" in token:
					constr_args = self.constraint_parser.parse_args(token[len("constr"):].split())	
					innerangr.print_constraint_history(active_index=constr_args.active_index)
				else:
					innerangr.print_constraint_history()

			#implement a "step until something happens"
class Shell:
	def __init__(self,binary=None):
		self.prompt = ""
		self.innerangr = innerangr.InnerAngr()
		self.binary = binary
		self.parser = Parser(self.binary)

	def parse(self,line):
		return self.parser.line_parse(line,self.innerangr)

	def run(self):
		title = "syrebro"
		while True:
			try:
				if (self.innerangr.loaded or self.innerangr.running):
					prompt = "*%s:%s %s>" % (title,self.innerangr.target,self.innerangr.prompt)
				else:
					prompt = "%s>" % (title)

				self.innerangr.display_execution_tree()
				_in = input(prompt)	
				time.sleep(1)
				self.parse(_in)
					
			except KeyboardInterrupt as k:
				pass
			"""
			except Exception as a:
				print("[*] %s" % (traceback.format_exc()))	
				break

			"""
if __name__ == "__main__":
	Shell().run()
