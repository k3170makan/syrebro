#!/usr/bin/python3
import traceback
import angr
import time
import random
import sys
import copy
import claripy
import argparse
import itertools
import networkx as nx
import logging

READ = "read"
WRITE = "write"
THREAD_LIMIT = 1
bss = []
data = []

global max_block_count
global max_callstack_depth
global global_access_states
global global_accesses
global pthread_calls
global memory_access
global _metrics

_metrics = False
_args = []

"""
	ACTIVE TEST: forward=True (pthread-forwarding)

	experiment in directed execution for identifying global addresss accesses
	- step forward only states that reach global memory accesses
	- optimizations:
		- records unique global access points
		- explores states to optimize coverage
		- uses bi-polarized directed symbolic exploration to find global memory access points
		- blends with nth-depth first path exploration: nth of the deepest paths are explored first
		- finer optimizations:
			- does not record global read events before a thread-create is encountered
			- first checks between each live thread to find races
	- experiments:
		* - collect some metrics on execution, see if we can tell when there's indication of state explosion
			-- execution time between steps (smoothness)
			-- number of states that are alive (activity)
			-- amount of memory being used (pressure)
			-- we want to show 
				* - increase in smoothness as activity increases and vice versa
				* - pressure may rise linearly throughout usage in a normal execution (need to collect data)
				 
"""
class nDFS(angr.ExplorationTechnique): #OPTIMIZATION: keep a variable number of paths alive
	"""
	Depth-first search with configurable alive-path limit
	
	Will only keep n paths active at a time, any others will be stashed in the 'deprioritized' stash.
	When we run out of active paths to step, we take the longest one from deferred and continue.
	"""
	#def __init__(self, limit=1,deferred_stash='deferred'):
	def __init__(self, limit=1,deprioritized_stash='deprioritized'):
		super(nDFS, self).__init__()
		self.limit = limit
		self._random = random.Random()
		self._random.seed(10)
		self.deprioritized_stash = deprioritized_stash

	def setup(self, simgr):
		if self.deprioritized_stash not in simgr.stashes:
			simgr.stashes[self.deprioritized_stash] = []
	def step(self, simgr, stash='active', **kwargs):
		simgr = simgr.step(stash=stash, **kwargs)
	
		#if age_average(simgr) > len(simgr.stashes['active']):
				
		if len(simgr.stashes[stash]) > self.limit:
			self._random.shuffle(simgr.stashes[stash])
			simgr.split(from_stash=stash, to_stash=self.deprioritized_stash, limit=self.limit)

		"""
		if len(simgr.stashes[stash]) == 0: #deferred state recycling
			#what do i do when we run out of states?
			if len(simgr.stashes[self.deferred_stash]) == 0:
				return simgr

			self._random.shuffle(simgr.stashes[stash])
			simgr.move(self.deferred_stash,stash)
			simgr.split(from_stash=stash, to_stash=self.deferred_stash, limit=self.limit)
			#we should reset the goals here as well
		"""
		return simgr

class LoopInfoPlugin(angr.SimStatePlugin):
	def __init__(self,count=0):
		self.visit_count = count;
	def copy(self, memo):
		return LoopInfoPlugin(count=0) 

class ThreadInfoPlugin(angr.SimStatePlugin):
	def __init__(self):
		self.prev_thread_id = 0
		self.current_thread_id = 1
		self.next_thread_id = 2
		
		self.TG = nx.DiGraph()
		self.cn = frozenset([0]) #im not sure what this is but it looks like a set of current nod[key] = self.accesses[key]#
		
		self.locks_held = set()
		
		self.accesses = {}
	
	def copy(self, memo):
		result = ThreadInfoPlugin()
		result.prev_thread_id = self.prev_thread_id
		result.current_thread_id = self.current_thread_id
		result.next_thread_id = self.next_thread_id
		
		result.TG = copy.deepcopy(self.TG)
		result.cn = copy.deepcopy(self.cn)
		
		result.locks_held = copy.deepcopy(self.locks_held)

		for key in self.accesses.keys():
			result.accesses[key] = self.accesses[key]

		return result

class pthread_create_hook(angr.SimProcedure):
	def run(self, nt, attr, start_rountine, arg):
		global memory_access

		state_ip = _eval_type(self.state, self.state.ip)
		if not(state_ip in memory_access): #OPTIMIZATION: make sure to only check unique states
			pthread_calls.append(state_ip)

		thread = self.state.solver.eval(nt)
		
		self.state.thread_info.prev_thead_id = self.state.thread_info.current_thread_id
		self.state.thread_info.current_thread_id = self.state.thread_info.next_thread_id
		self.state.thread_info.next_thread_id += 1
		
		#print("[!>] enter thread: %s -> %s" % (repr(self.state.thread_info.prev_thread_id),
		#					repr(self.state.thread_info.current_thread_id)))	

		src_node = self.state.thread_info.cn
		
		tmp = set(src_node)
		tmp.add(self.state.thread_info.current_thread_id)
		dest_node = frozenset(tmp)
		
		self.state.thread_info.TG.add_edge(src_node, dest_node, create=self.state.thread_info.current_thread_id)
		self.state.thread_info.cn = dest_node
		
		self.state.mem[thread].uint64_t = self.state.thread_info.current_thread_id
		self.call(start_rountine, (arg,), 'on_return')
	def on_return(self, thread, attr, start_rountine, arg):
		prev = self.state.thread_info.current_thread_id
		self.state.thread_info.current_thread_id = self.state.thread_info.prev_thread_id
		self.state.thread_info.prev_thread_id = prev
		
		#print("[!>] leave thread: %s -> %s" % (repr(self.state.thread_info.prev_thread_id),
		#					repr(self.state.thread_info.current_thread_id)))	

		self.ret(self.state.solver.BVV(0, self.state.arch.bits))
		#self.ret(0)
	
class pthread_join_hook(angr.SimProcedure):
	def run(self, thread, retval):
		global memory_access

		state_ip = _eval_type(self.state, self.state.ip)
		if not(state_ip in memory_access): #OPTIMIZATION: make sure to only check unique states
			pthread_calls.append(state_ip)

		joined_id = self.state.solver.eval(thread.to_claripy())

		#print("[!>] join thread %d" % (joined_id))	

		src_node = self.state.thread_info.cn
		tmp = set(src_node)
		tmp.remove(joined_id)
		dest_node = frozenset(tmp)
		
		self.state.thread_info.TG.add_edge(src_node, dest_node, join=joined_id)
		self.state.thread_info.cn = dest_node
		
class pthread_mutex_lock_hook(angr.SimProcedure):
	def run(self, mutex):
		global memory_access

		state_ip = _eval_type(self.state, self.state.ip)
		if not(state_ip in memory_access): #OPTIMIZATION: make sure to only check unique states
			pthread_calls.append(state_ip)

		mutex_address = self.state.solver.eval(mutex.to_claripy())
		#print("[!>] thread %d : lock mutex <%d>" % 
		#	(self.state.thread_info.current_thread_id, mutex_address))

		self.state.thread_info.locks_held.add(mutex_address)


class pthread_mutex_unlock_hook(angr.SimProcedure):
	def run(self, mutex):
		global memory_access

		state_ip = _eval_type(self.state, self.state.ip)
		if not(state_ip in memory_access): #OPTIMIZATION: make sure to only check unique states
			pthread_calls.append(state_ip)

		mutex_address = self.state.solver.eval(mutex.to_claripy())
		
		#print("[!>] thread %d : unlock mutex <%d>" % 
		#	(self.state.thread_info.current_thread_id, mutex_address))
		try:
			self.state.thread_info.locks_held.remove(mutex_address) #should note this problem
		except KeyError as key:
			#problem removing this mutex? how?
			#print("[unlock mutex callback x] %s\n%s" % (repr(key),traceback.format_exc()))
			pass
	
class DivertExecuteAddressGoal(angr.exploration_techniques.director.BaseGoal):
	def __init__(self, addr):
		super(DivertExecuteAddressGoal,self).__init__('diverted_execute_address')
		self.addr = addr
		self.peek_blocks = 10
		self.max_callstack_depth = 0

	def __repr__(self):
		return "<DivertExecuteAddressGoal targeting %#x>" % self.addr #one goal for each address

	def check(self, cfg, state, peek_blocks):
		"""we are diverting from a goal in order to discover more access points"""

		self.peek_blocks = peek_blocks
		node = self._get_cfg_node(cfg,state)
		if node is None:
			return False
		#if state.loop_info.visit_count > 10:
		#	self.peek_blocks = 1
		#not sure if this simple algorithm will do, might need to speed this up
		for src,dst in self._dfs_edges(cfg.graph, node, max_steps=self.peek_blocks):
			if src.addr == self.addr or dst.addr == self.addr:
				#move this to the deferred states
				self.update_callstack_depth(state)
				return False #since this reaches we will divert execution away
		#if self.check_state_depth(state):
		#	self.update_callstack_depth(state)
		#	return True
		return False

	def update_callstack_depth(self,state):
		if len(state.callstack) > self.max_callstack_depth:
			self.max_callstack_depth = len(state.callstack)

	def check_state_depth(self,state):
		return len(state.callstack) > self.max_callstack_depth

	def check_state(self, state):
		#return state.addr != self.addr or self.check_state_depth(state)
		return state.addr != self.addr

def is_global(address):
        _address_value = 0
        if bss != [] and data != []:
                if type(address) == type(1):
                        _address_value = address
                if ((_address_value > bss[0] and _address_value < bss[0]+bss[1]) or
                        (_address_value > data[0] and _address_value < data[0]+data[1])):
                        return True
        return False

def _eval_type(state,_val):
	if _val != None and (type(_val) != type(1)):
		try:
			return state.solver.eval(_val)
		except Exception as e:
			#print("[_eval_type x] %s\n%s" % (repr(e),traceback.format_exc()))
			pass
	return _val

def __print_access(state,_type,address,length,ip,cn):
        #return #added this to simplify output for now
	if address != None and is_global(address) and not(_metrics):
		print("\t\t-- %s {%s} @(%s) [%s] |%s| /%d/" % (_type,ip,hex(address),length,repr(cn),len(state.callstack)),end='\r')

def _syscall_callback(state):
	syscall_name = state.inspect.syscall_name
	state_ip = _eval_type(state,state.ip)
	print("\t\t-- %s {%s}" % (syscall_name,state_ip))

def update_accesses(state):
		global max_callstack_depth
		if len(state.callstack) > max_callstack_depth:
			max_callstack_depth = len(state.callstack)
		for addr in state.thread_info.accesses.keys():
				#print(type(addr))
				access = state.thread_info.accesses[addr]
				try:
					#we should record this only if its a write from a new thread we don't have, but for now keep it naive
					global_accesses[addr].append(access)
				except KeyError as key:
					#print("[update_accesses x] %s\n%s" % (repr(key),traceback.format_exc()))
					global_accesses[addr] = [access]


def _mem_read_callback(state):
	global memory_access
	global global_access_states

	address = _eval_type(state, state.inspect.mem_read_address)
	length = _eval_type(state, state.inspect.mem_read_length)
	state_ip = _eval_type(state,state.ip)
	if len(state.thread_info.cn) >= THREAD_LIMIT and (is_global(address)) and state.satisfiable(): #OPTIMIZATION: Check if more than one thread is reported to be active
		if not(state_ip in memory_access):# and len(state.callstack) > max_callstack_depth: #OPTIMIZATION: make sure to only check unique states
			
			memory_access.append(state_ip)
			global_access_states.append(state)
			update_accesses(state)
			#__print_access(state,"mem_read",address,length,state.ip,len(state.thread_info.cn))

		for i in range(length):
			addr = address + i
			access = (state.thread_info.current_thread_id, state_ip, addr, READ,
					frozenset(state.thread_info.locks_held), state.thread_info.cn)
			if addr not in state.thread_info.accesses:
				state.thread_info.accesses[addr] = set()
			state.thread_info.accesses[addr].add(access)
			#thread_info_check()

def _mem_write_callback(state):
	global memory_access
	global global_access_states

	address = _eval_type(state, state.inspect.mem_write_address)
	length =  _eval_type(state, state.inspect.mem_write_length)
	state_ip = _eval_type(state,state.ip)
	if len(state.thread_info.cn) >= THREAD_LIMIT and is_global(address) and state.satisfiable():
		if not(state_ip in memory_access):# and len(state.callstack) > max_callstack_depth:
			memory_access.append(state_ip)
			global_access_states.append(state)
			update_accesses(state)

			#__print_access(state,"mem_write",address,length,state.ip,len(state.thread_info.cn))
			#thread_info_check()
		for i in range(length):
			addr = address + i
			access = (state.thread_info.current_thread_id, state_ip, addr, WRITE,
					frozenset(state.thread_info.locks_held), state.thread_info.cn)
			if addr not in state.thread_info.accesses:
				state.thread_info.accesses[addr] = set()
			state.thread_info.accesses[addr].add(access)

def eval_args(state):
	for index,arg in enumerate(_args):
		print("\t\t|--- arg_%s :=> %s " % (index,state.solver.eval(arg, cast_to=bytes)))

def thread_info_check():
	
	for addr in global_accesses.keys():
		try:
			accesses = global_accesses[addr]
			for access in accesses:
				combinations = itertools.combinations(access,2)
				for combo in combinations:
					access_1 = combo[0]
					access_2 = combo[1]
					if access_1[0] != access_2[0] and (access_1[3] == WRITE or access_2[3] == WRITE): #read/write check
						#if access_1[4].intersection(access_2[4]) >= 0: #lock check
						#if len(access_1[4]) == 0 or len(access_2[4]) == 0: #lock check
						print("*{%s} :=> %s %s" % 
							(hex(addr),access_1,access_2))

		except ReferenceError as ref:
			#print("[thread info check callback x] %s\n%s" % (repr(ref),traceback.format_exc()))
			pass
	"""
	for _state in global_access_states:
		try:
			for addr in _state.thread_info.accesses.keys():
				access = _state.thread_info.accesses[addr]
				print("\t\t:=> accesses {%s} : %s" % 
							(hex(addr),
							access))
			#print()
		except ReferenceError as ref:
			#print("[thread info check callback x] %s\n%s" % (repr(ref),traceback.format_exc()))
			pass
	"""
	"""
	for addr in addr_index.keys():
		print("\t\t:=> accesses {%s} : %s" % 
					(hex(addr),
					access_index[addr]))
	"""
def age_average(simmgr):
	return float(sum([_state.loop_info.visit_count for _state in simmgr.stashes['active']])/len(simmgr.stashes['active']))

class DirectedExecutionForGlobalMemoryAccessPoints(angr.Analysis):
	def __init__(self,binaryname="binary",
							initial_state=None,
							project=None,
							steps=500,
							nlimit=1,
							use_dm=False,
							exhaustive=False,
							metrics=False,
							unicorn=False,
							stochastic=False):

		self.binary_name=binaryname
		self.use_dm = use_dm
		self.nlimit = nlimit #number of paths to keep alive
		self.steps = steps #bounded symbolic execution
		self.project = project 
		self.metrics = metrics
		self.goals = []
		self.target_functions = [] #static analysis component 
		self.found_targets = False
		self.initial_state = initial_state
		self.CFG_done = False
		self.cfg = None
		self.using_dm = False
		self.diverted = False
		self.exhaustive = exhaustive
		self.stochastic = stochastic
		self.unicorn = unicorn	
		self.initial_state.register_plugin("loop_info", LoopInfoPlugin())		
		self.initial_state.register_plugin("thread_info", ThreadInfoPlugin())		
		self.init_simulation()


	def add_unicorn(self,state):
		for opt in angr.options.unicorn:
			state.options.add(opt)
		#state.options.add("UNICORN_AGGRESSIVE_CONCRETIZATION")

	def init_simulation(self):
		self.dm = angr.exploration_techniques.Director(cfg_keep_states=True)
		if self.unicorn:
			if not(self.metrics):
				print("[>] using unicorn engine...")
			self.add_unicorn(self.initial_state) 

		self.simmgr = self.project.factory.simulation_manager(self.initial_state)#, veritesting=True)
		self.simmgr.use_technique(angr.exploration_techniques.MemoryWatcher(memory_stash='deprioritized'))
		if self.stochastic:
			if not(self.metrics):
				print("[>] using stochastic search...")
			self.simmgr.use_technique(angr.exploration_techniques.stochastic.StochasticSearch(start_state=self.initial_state))	
		self.simmgr.use_technique(nDFS(self.nlimit))	
		
			
	def _step(self):
		global max_callstack_depth
		global max_block_count
		try:
			self.simmgr.step()
			for state in self.simmgr.stashes['active']:
				if len(state.callstack) > max_callstack_depth:
					max_callstack_depth = len(state.callstack)
					#self.dm.max_callstack_depth = max_callstack_depth
				if state.history.block_count > max_block_count:
					max_block_count = state.history.block_count

				"""
				if (self.unicorn):
						self.add_unicorn(state)
				"""
				state.loop_info.visit_count += 1	

		except IndexError as i:
			return
		except Exception as e:
			print("[DirectdExecutionForGlobalMemoryAccessPoints._step x] %s\n%s" % (repr(e),traceback.format_exc()))
			pass	

	def derive_cfg(self,avoids=[]):
		self.cfg = self.project.analyses.CFGEmulated(call_depth=1,
					context_sensitivity_level=1,
					keep_state=True,
					#state_add_options=angr.sim_options.refs, 
					max_iterations=1)
		#self.cfg = self.project.analyses.CFGEmulated() #testing whether these options actually improve our exploration 
		self.dm._cfg = self.cfg
		return;
	def console_println(self,i,start,end,average):
		global max_callstack_depth
		global memory_access
		global max_block_count

		if self.metrics:
			#sys.stdout.write("%s %s %s %s %s %s\n" % (i,len(self.simmgr.stashes['active']),end-start,average,len(memory_access),max_callstack_depth))
			if i == 1:
				sys.stdout.write("0:n, 1:step_time, 2:avg_step_time, 3:gmaps, 4:callstack_depth, 5:block_count, 6:st_active, 7:st_deprio, 8:st_dead, 9:st_uncon, 10:st_unsat, 11:st_error\n")
			sys.stdout.write("%s %s %s %s %s %s %s %s %s %s %s %s\n" % (i,
											end-start,
											average,
											len(memory_access),
											max_callstack_depth,
											max_block_count,
											len(self.simmgr.stashes['active']),
											len(self.simmgr.stashes['deprioritized']),
											len(self.simmgr.stashes['deadended']),
											len(self.simmgr.stashes['unconstrained']),
											len(self.simmgr.stashes['unsatisfied']),
											len(self.simmgr.stashes['errored'])))
			
			#sys.stdout.write("%s %s %s %s %s\n" % (i,len(self.simmgr.stashes['active']),end-start,average,len(memory_access)))
			sys.stdout.flush()
			#print("%s %s %s %s %s" % (i,len(self.simmgr.stashes['active']),end-start,average,len(memory_access)))
		else:
			try:
				stashes = "".join(["%s:%s " % (stash_name[:6],len(self.simmgr.stashes[stash_name])) for stash_name in self.simmgr.stashes]).lstrip(" ")
				print("[%s>] {%s} *%d :%d ( %s) %s *%s gmaps:[%s] <%f> %f" % (self.binary_name,
											i,
											max_callstack_depth,
											max_block_count,
											stashes,
											self.simmgr.stashes['active'][0],
											age_average(self.simmgr),
											len(memory_access),
										end-start,average),end='\r')
			except:
				pass
		return;
	def run(self):
		global memory_access
		global global_access_states
		global max_callstack_depth
		global pthread_calls
		global _metrics

		if self.use_dm:
			self._init_goals()
			self._init_target_functions()

			if not(self.metrics):
				print("[>] deriving CFG...",end='')
			self.derive_cfg()
			self.simmgr.use_technique(self.dm)
			self.simmgr.use_technique(angr.exploration_techniques.stochastic.StochasticSearch(start_state=self.initial_state))	
			self.CFG_done = True
			if not(self.metrics):
				print("done")
				print("[>] using directed symbolic exploration towards => %s" % (self.target_functions))

		self._init_breakpoints()
		average = 0
		total = 0
		i = 0
		while i < self.steps or (len(self.simmgr.stashes['deferred']) > 0	and len(self.simmgr.stashes['active']) > 0):
			i+=1
			start = time.time()
			self._step()
			end = time.time()
			total += end-start

			if (i > 0):
				average = total/(i*1.0)
			else:
				average = 0

			self.console_println(i,start,end,average)

			if self.use_dm:	
				if memory_access != []:
					if not(self.CFG_done):
						self.derive_cfg()
						self.CFG_done = True

					for addr in memory_access:
						if not(addr in self.goals):
							self.append_goal(addr,forward=False)				
					for addr in pthread_calls:
						if not(addr in self.goals):
							self.append_goal(addr,forward=True)	#please note this is experimental! under going tests

					if self.using_dm == False:
						if not(self.metrics):
							print("!> switching to directed symbolic execution...")
						if not(self.CFG_done):
							self.derive_cfg()
						self.simmgr.use_technique(self.dm) 
						if self.stochastic:
							self.simmgr.use_technique(angr.exploration_techniques.stochastic.StochasticSearch(start_state=self.initial_state)) 
						self.using_dm = True
			if len(self.simmgr.stashes['deadended']) > 0:
				for states in self.simmgr.stashes['deadended']:
					thread_info_check()			
		if self.exhaustive:
			self.reset()
			self.exhaustive = False
		else:
			thread_info_check()	
	def longest_callchain(self,state_list=[]):
		
		self.longest_callchain = 0
		self.furthest_state = None
		for state in state_list:
			if len(state.callchain) > self.longest_callchain:
				self.longest_callchain = state.callchain 
				self.furthest_state = state
		return self.furthest_state

	def reset(self):
		state = self.longest_callchain(global_access_states)
		if state != None:
			self.initial_state = state
			self.init_simulation()
			self.simmgr.stashes['deprioritized'].append(global_access_states)

			max_callstack_depth = 0
			memory_access = []
			global_access_states = []
			self.run()	
		#restart from the 
	def _init_breakpoints(self):
		#print("[>] adding breakpoints...")
		self.initial_state.inspect.b("mem_write",when=angr.BP_AFTER,
							action=_mem_write_callback)

		self.initial_state.inspect.b("mem_read",when=angr.BP_AFTER,
							action=_mem_read_callback)

		#self.initial_state.inspect.b("syscall",when=angr.BP_AFTER,
		#					action=_syscall_callback)
		
				
	def _init_function_names(self):
		self.project.hook_symbol("pthread_create",pthread_create_hook())	
		self.project.hook_symbol("pthread_mutex_lock",pthread_mutex_lock_hook())	
		self.project.hook_symbol("pthread_mutex_unlock",pthread_mutex_unlock_hook())	
		self.project.hook_symbol("pthread_join",pthread_join_hook())	

		self.target_function_names = ["pthread_create",
						"pthread_mutex_lock",
						"pthread_mutex_unlock",
						"pthread_join"]
		return

	def append_goal(self,addr,forward=False):
		_goal = DivertExecuteAddressGoal(addr)
		self.goals.append(addr) #goals we are already diverting from
		if not(forward):
			#print("*>\t adding diverted goal => [%s]" % (hex(addr)))
			if self.diverted:
				self.dm.add_goal(_goal)
			else:
				self.dm = angr.exploration_techniques.Director(cfg_keep_states=True, goals=[_goal])
				self.diverted = True
		else:
			#print("*>\t adding forward goal => [%s]" % (hex(addr)))
			_goal = angr.exploration_techniques.ExecuteAddressGoal(addr)
			self.dm.add_goal(_goal)
			
	
	def _init_target_functions(self):
		self._init_function_names()
		#print("*> function names => %s" % (self.target_function_names))
		for function_name in self.target_function_names:
			_function = self.project.kb.functions.function(name=function_name,plt=True)
			#print("*> function names => %s" % (repr(_function)))
			if _function != None:
				#print("*> found function => %s @[%s]" % (function_name, _function))	
				self.target_functions.append(_function)

		if len(self.target_functions) > 0:
			self.found_targets = True	

	def _init_goals(self):
		for function in self.target_functions:
			_goal = angr.explotation_techniques.ExecuteAddressGoal(function.addr)
			#_goal = angr.exploration_techniques.CallFunctionGoal(function,[]) 
			self.goals.append(_goal)	
			self.dm.add_goal(_goal)

angr.AnalysesHub.register_default("DirectedExecutionForGlobalMemoryAccessPoints",DirectedExecutionForGlobalMemoryAccessPoints)


if __name__=="__main__":

	parser = argparse.ArgumentParser()
	parser.add_argument("-b","--binary",help="Binary to parse")	
	parser.add_argument("-v","--verbose",help="verbosity level, {INFO|WARNING|CRITICAL|ERROR}, default=ERROR",default="ERROR")	
	parser.add_argument("-c","--arg_count",help="Number of arguments",default=3)	
	parser.add_argument("-l","--arg_length",help="Bit length of each argument",default=32)	
	parser.add_argument("-L","--nlimit",help="number of paths to keep alive for DFS",default=1)	
	parser.add_argument("-s","--step_count",help="Number of symbolic execution steps",default=1024)	
	parser.add_argument("-d","--directed",help="use directed symbolic execution to find program points",action='store_true',default=False)	
	parser.add_argument("-e","--exhaustive",help="execute at least 1024 steps but keep going until the deferred stash is exhausted",action='store_true',default=False)	
	parser.add_argument("-m","--metrics",help="metrics mode which instead of verbose output just prints out data profiling",action='store_true',default=False)
	parser.add_argument("-u","--unicorn",help="use unicorn engine",action='store_true',default=False)
	parser.add_argument("--stochastic",help="use stochastic search",action='store_true',default=False)
	args = parser.parse_args()

	binary_name = args.binary
	arg_count = int(args.arg_count)
	arg_length = int(args.arg_length)
	step_count = int(args.step_count)
	nlimit = int(args.nlimit)
	if args.metrics:
		logging.getLogger('angr').propagate = False
		_metrics = False
	else:
		logging.getLogger('angr').setLevel(args.verbose)

		
	project = angr.Project(binary_name,load_options={"auto_load_libs":False});
	for section in project.loader.main_object.sections:
		if section.name == ".data":
			bss = [section.vaddr, section.memsize]
		elif section.name == ".bss":
			data = [section.vaddr, section.memsize]

	if not(_metrics):
		print("binary :=> %s" % (binary_name))
		print("\targ_count :=> %s" % (arg_count))
		print("\targ_length :=> %s" % (arg_length))
		if len(bss) != 0:
			print("\tbss :=> %s" % (hex(bss[0])))
			print("\tdata :=> %s" % (hex(data[0])))
		else:
			print("\tbss :=> %s" % (bss))
			print("\tdata :=> %s" % (data))

	_args = [claripy.BVS("arg%s" % (i), 8*arg_length) for i in range(arg_count)]
	entry_state = project.factory.entry_state(args=_args)
	max_callstack_depth = 0
	memory_access = [] 
	pthread_calls = []
	global_access_states = []
	global_accesses = {}
	max_block_count = 0

	global_address_access = project.analyses.DirectedExecutionForGlobalMemoryAccessPoints(binaryname=binary_name,
									initial_state=entry_state,
									project=project,
									steps=step_count,
									nlimit=nlimit,
									use_dm=args.directed==True,
									exhaustive=args.exhaustive==True,
									metrics=args.metrics==True,
									unicorn=args.unicorn==True,
									stochastic=args.stochastic==True)	

	global_address_access.run()
