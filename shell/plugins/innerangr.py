#!/usr/bin/python3

import traceback
import angr
import random
import claripy
import asciitree
import networkx as nx
import matplotlib.pyplot as plt
import time
import subprocess
import os
import glob

from PIL import Image
"""
Angr internal instance for the shell

"""

class InnerAngr:
	def __init__(self,target=None,
							load_options={"auto_load_libs":False,"main_opts":{"base_addr":0x555555554000}},
							args_length=16,args_size=16,args_stdin_length=128,step_count=4096,use_directed=False,custom_exploration=None):

		#check that file exists?
		self.seen_states = []
		self.last_seen = 0
		self.execution_dict = {}
		self.execution_edgelist = {}
		self.execution_tree = asciitree.LeftAligned()
		self.execution_digraph = nx.DiGraph()

		self.running = False
		self.active = 0
		self.loaded = False
		self.load_options = load_options	
		self.project = None
		self.target = target
		self.simulation_manager = None
		self.prompt = ""

		if (target):
			self.project = angr.Project(target,load_options=self.load_options)

		self.sym_stdin = claripy.BVS("/dev/stdin",args_length, annotations=(claripy.Annotation(),))
		self.sym_args = [claripy.BVS("argv_%s" % (i),args_size,annotations=(claripy.Annotation(),)) for i in range(args_length)]
		self.step_count = step_count

		if (custom_exploration):
			self.custom_exploration = custom_exploration
		self.use_directed = use_directed
		self.state_history = []
		self.steps = 0
		if (target):
			self.init_simulation()
	def set_target(self,target=None):
		if (target):
			self.target =target	
			self.project = angr.Project(self.target,load_options=self.load_options)
			self.init_simulation()

	def init_simulation(self):
		self.entry_state = self.project.factory.entry_state(args=self.sym_args,stdin=self.sym_stdin)	
		if not(self.project):
			self.project = angr.Project(self.target,load_options=self.load_options)
		self.simulation_manager = self.project.factory.simulation_manager(self.entry_state)
		#self.simulation_manager.use_technique(nDFS())
		self.execution_dict = {}
		self.execution_edgelist = {}
		self.execution_tree = asciitree.LeftAligned()
		self.execution_digraph = nx.DiGraph()
		self.steps = 0

		self.loaded=True
		

	def step_state(self):
		self.running = True		
		try:
			last_active = self.simulation_manager.stashes['active'][0]
			last_active_ip = self.eval(last_active,last_active.ip)
			self.last_seen = hex(last_active_ip)
		except IndexError:
			pass
		if self.simulation_manager:
			if (self.steps < self.step_count):
				self.simulation_manager.step()
				self.steps += 1
		
		else:
			raise Exception("InnerAngr: Simulation Manager is not initalized!")


		try:
			last_active = self.simulation_manager.stashes['active'][0]
			self.prompt = "(%d)[a:%d d:%d]{%s}" % (self.steps,len(self.simulation_manager.stashes['active']),len(self.simulation_manager.stashes['deadended']),last_active)
			self.update_execution_dict()
		except IndexError:
			return ""
		return self.prompt

	def set_active_index(self,index=0):
		self.last_actve = self.active
		self.active = index

	def update_execution_dict(self):
		
		actives = self.simulation_manager.stashes['active']
		try:
			for index, state in enumerate(actives):
				state_history = [hist for hist in state.history.parents]
				prev = state_history[0]
				prev_ip = hex(self.eval_histip(prev))

				if len(state_history) <= 1:
					self.execution_dict = {"%s" % (prev_ip): {}}
					return
				
				for statehist in state_history[1:]:
					cur = statehist
					cur_ip = hex(self.eval_histip(cur))
					try:
						if not(cur_ip in self.execution_edgelist[prev_ip]):
							self.execution_edgelist[prev_ip].append(cur_ip)
							self.execution_digraph.add_edge(prev_ip,cur_ip)
					except KeyError as ke:
							self.execution_edgelist[prev_ip] = [cur_ip]
							#print("(%s) --> (%s)" % (prev_ip,cur_ip))
							self.execution_digraph.add_edge(prev_ip,cur_ip)

					prev = cur
					prev_ip = hex(self.eval_histip(prev))

		except Exception as e:
			print("%s" % (e),traceback.format_exc())
		#print(self.execution_tree(self.execution_dict),end='\r')
		self.show_edge_list()

	def show_edge_list(self):
		for index,key in enumerate(self.execution_edgelist):
			print("[%d] <%s> --> <%s>" % (index,key,self.execution_edgelist[key]));		
	
	def plot_svg_drigraph(self,filename):
			self.plot_digraph(filename+".svg")

	def plot_png_drigraph(self,filename):
			self.plot_digraph(filename+".png")

	def plot_jpg_drigraph(self,filename):
			self.plot_digraph(filename+".jpg")


	def plot_digraph(self,filename="syrebro_digraph.svg",steps=0,dpi=1500,labels=False):

		options ={ 'node_size':60, 
						'width':1,
						'with_labels':labels,
						'font_size':7}

		for i in range(steps):
			self.step_state()
			
		nx.draw(self.execution_digraph,**options)	
		plt.savefig(filename,orientation="landscape",dpi=dpi)
	
	def plot_gifdigraph(self,filename="syrebro",steps=10):
		filenames = ["%s_frame_%d.png" % (filename,i) for i in range(steps)]
		for frame_filename in filenames:
			self.plot_digraph(filename=frame_filename,steps=0)
			self.step_state()

		filenames = sorted(filenames)
		img, *imgs = [Image.open(f) for f in filenames]
		print("[*] saving gif to '%s'" % (filename+"_stategraph.gif"))
		img.save(fp=filename+"_stategraph.gif", format='GIF', append_images=imgs,
         				save_all=True, duration=150, loop=0)


	def is_seen_state(self,state):
		return state in self.seen_states 

	def display_execution_tree(self):
		if (len(self.execution_dict) != 0):
			print(self.execution_tree(self.execution_dict))

	def eval(self,state,val):
			try:
				return state.solver.eval(val)
			except ReferenceError as ref:
				print("[x] problematic state :=> %s" % (repr(state)))
				raise ref

	def eval_stateip(self,state):
		try:
			return state.solver.eval(state.ip)
		except ReferenceError as ref:
			print("[x] problematic state :=> %s" % (repr(state)))
			return self.eval(state,state.addr)
			
	def eval_histip(self,hist):
		try:
			return self.eval(hist.state,hist.state.ip)
		except ReferenceError as ref:
			#print("[x] problematic history :=> %s (addr :=> %s)" % (repr(hist),hist.addr))
			return hist.addr

	def step_until(self,actives=-1,deadended=-1,targets="",avoids=-1):	
		if actives != -1:
			print("[*] executing until '%s' actives states are found" % (actives))
			while len(self.simulation_manager.stashes['active']) < int(actives):
				self.step_state()
				print(self.prompt,end='\r')

		if deadended != -1:
			print("[*] executing until '%s' deadended states are found" % (deadended))
			while len(self.simulation_manager.stashes['deadended']) < int(deadended):
				self.step_state()
				print(self.prompt,end='\r')

		if targets != "":
			target_list = targets.split(',')
			
			pass

	def print_state(self):
		return	
	def print_state_codeblock(self):
		return	

	def print_constraint_history(self, active_index=0,stash="active"):
		active = self.simulation_manager.stashes[stash]
		
		for index,statehist in enumerate(active.history.parents):
			for constraint in statehist.state.solver.constraints:
				print("\t* - %s" % (constraint))
			print("}")


		"""
		for index,state in enumerate(self.state_history):
			print("* [%d]: %s {\n" % (index,active.ip),end='')
			for constraint in state[self.active].solver.constraints:
				print("\t* - %s" % (constraint))
			print("}")

		"""
		
	def print_history(self, active_index=0):
		for index,statehist in enumerate(self.simulation_manager.stashes['active'][active_index].history.parents):
			string = ""
			try:
			
				string = "[%d]: %s {\n" % (index,statehist)
				for regname in ["rip","rsp","rbp","rdi","rsi","rax","rbx","rdx","r8","r9","r10","r11","r12","r13","r14","r15"]:
					string += "\t - %s=%s\n" % (regname,repr(statehist.state.regs.get(regname)))
				string += "}"
				print(string)
			except ReferenceError:
				print("[%d]: %s { %s }" % (index,statehist," ... "))

class nDFS(angr.ExplorationTechnique): #OPTIMIZATION: keep a variable number of paths alive
	"""
	Depth-first search with configurable alive-path limit
	
	Will only keep n paths active at a time, any others will be stashed in the 'deferred' stash.
	When we run out of active paths to step, we take the longest one from deferred and continue.
	"""
	def __init__(self, limit=1,deferred_stash='deferred'):
		super(nDFS, self).__init__()
		self.limit = limit
		self._random = random.Random()
		self._random.seed(10)
		self.deferred_stash = deferred_stash

	def setup(self, simgr):
		if self.deferred_stash not in simgr.stashes:
			simgr.stashes[self.deferred_stash] = []
	def step(self, simgr, stash='active', **kwargs):
		simgr = simgr.step(stash=stash, **kwargs)
	
		#if age_average(simgr) > len(simgr.stashes['active']):
				
		if len(simgr.stashes[stash]) > self.limit:
			self._random.shuffle(simgr.stashes[stash])
			simgr.split(from_stash=stash, to_stash=self.deferred_stash, limit=self.limit)

		if len(simgr.stashes[stash]) == 0:
			#what do i do when we run out of states?
			if len(simgr.stashes[self.deferred_stash]) == 0:
				return simgr

			self._random.shuffle(simgr.stashes[stash])
			simgr.move(self.deferred_stash,stash)
			simgr.split(from_stash=stash, to_stash=self.deferred_stash, limit=self.limit)
		return simgr

