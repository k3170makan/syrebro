#!/usr/bin/python3
from shell import Shell
import argparse

"""
TODO:
	 - how can we display constraints? (as edge labels in the graph? as graphs on their own?)
	 - how can we specify configuration for exploration techniques?
	 - can we plot images on a black backgrond so fabulous can diplay them internmal?
	 - implement stepping according to a "step count" option for the next command
	 - finish implementing the --graph_* options for show
	 -  

"""
if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-b","--binary",help="Binary to execute symbolically",default=None)
	parser.add_argument("--graph",help="Binary to execute symbolically",default=None)
	parser.add_argument("--graph_png",help="filename to use for png output",default=None)
	parser.add_argument("--graph_svg",help="filename to use for svg output",default=None)
	parser.add_argument("--graph_jpg",help="filename to use for jpg output",default=None)
	args = parser.parse_args()
	Shell(binary=args.binary).run()
